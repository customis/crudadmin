<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 19.11.2015
 * Time: 13:59
 */

namespace CustomIS\CrudAdminBundle\Request;


use CustomIS\CrudAdminBundle\Admin\Admin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminParamConverter implements ParamConverterInterface
{
    /**
     * @var ContainerInterface
     */
    private $serviceContainer;

    /**
     * AdminParamConverter constructor.
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
    }


    public function apply(Request $request, ParamConverter $configuration)
    {
        if (!$request->attributes->has('type'))
        {
            throw new NotFoundHttpException('Nebyl předán typ entity');
        }

        $type = $request->attributes->get('type');
        if ($this->serviceContainer->has(sprintf('crudadmin.admin.%s', $type)))
        {
            $admin = $this->serviceContainer->get(sprintf('crudadmin.admin.%s', $type));
            $request->attributes->set($configuration->getName(), $admin);
            return true;
        }
        else
        {
            throw new NotFoundHttpException('Neznámý typ entity "' . $type . '"');
            return false;
        }
    }

    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === Admin::class;
    }

}