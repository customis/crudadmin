<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 12.11.2015
 * Time: 15:36
 */

namespace CustomIS\CrudAdminBundle\Admin;


use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Form\FormInterface;

class Admin
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $entity;

    /**
     * @var ManagerRegistry
     */
    private $doctrineManager;

    /**
     * @var FormInterface
     */
    private $form;

    /**
     * @var String
     */
    private $title;

    /**
     * @var Column[]
     */
    private $columns = [];

    /**
     * @var string
     */
    private $repositoryMethod = 'findBy';

    /**
     * @var array
     */
    private $orderBy = [];

    /**
     * Admin constructor.
     * @param string $name
     * @param string $entity
     * @param $doctrineManager
     */
    public function __construct($name, $entity, ManagerRegistry $doctrineManager)
    {
        $this->name = $name;
        $this->entity = $entity;
        $this->doctrineManager = $doctrineManager;
    }

    /**
     * @param FormInterface $form
     */
    public function setForm(FormInterface $form)
    {
        $this->form = $form;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return String
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function addColumn(Column $column)
    {
        $column->setAdmin($this);
        $this->columns[] = $column;
    }

    /**
     * @return Column[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return string
     */
    public function getRepositoryMethod()
    {
        return $this->repositoryMethod;
    }

    /**
     * @param string $repositoryMethod
     */
    public function setRepositoryMethod($repositoryMethod)
    {
        $this->repositoryMethod = $repositoryMethod;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @return ManagerRegistry
     */
    public function getDoctrineManager()
    {
        return $this->doctrineManager;
    }

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $this->orderBy = $orderBy;
    }

    public function hasForm()
    {
        return $this->form !== null;
    }
}