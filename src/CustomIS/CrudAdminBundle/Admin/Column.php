<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 19.11.2015
 * Time: 13:31
 */

namespace CustomIS\CrudAdminBundle\Admin;


use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyAccess\PropertyPath;

class Column
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string|null
     */
    private $property;

    /**
     * @var Admin
     */
    private $admin;

    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $format;

    /**
     * Column constructor.
     * @param string $name
     * @param string $label
     * @param PropertyAccessor $propertyAccessor
     * @param string $format
     * @param boolean $editable
     */
    public function __construct($name, $label, PropertyAccessor $propertyAccessor, $format)
    {
        $this->name = $name;
        $this->label = $label;
        $this->propertyAccessor = $propertyAccessor;
        $this->format = $format;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return null|string
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param null|string $property
     */
    public function setProperty($property)
    {
        $this->property = $property;
    }

    /**
     * @param Admin $admin
     */
    public function setAdmin(Admin $admin)
    {
        $this->admin = $admin;
    }

    public function getValue($entityObject)
    {
        return $this->propertyAccessor->getValue($entityObject, $this->getProperty() ? $this->getProperty() : $this->getName());
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

}