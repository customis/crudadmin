<?php

namespace CustomIS\CrudAdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('custom_is_crud_admin');

        $rootNode
            ->children()
                ->arrayNode('entities')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('title')
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                            ->scalarNode('tag')
                                ->cannotBeEmpty()
                            ->end()
                            ->scalarNode('entity')
                                ->isRequired()
                                ->cannotBeEmpty()
                                ->validate()
                                    ->ifTrue(function ($value) {
                                        return !class_exists($value);
                                    })
                                    ->thenInvalid("Neexistující třida entity (%s).")
                                ->end()
                            ->end()
                            ->arrayNode('list')
                                ->children()
                                    ->scalarNode('repositoryMethod')
                                        ->cannotBeEmpty()
                                    ->end()
                                    ->scalarNode("role")
                                        ->cannotBeEmpty()
                                    ->end()
                                    ->arrayNode('columns')
                                        ->prototype('array')
                                            ->children()
                                                ->scalarNode("label")
                                                    ->isRequired()
                                                    ->cannotBeEmpty()
                                                ->end()
                                                ->scalarNode("property")
                                                    ->cannotBeEmpty()
                                                ->end()
                                                ->scalarNode("type")
                                                    ->cannotBeEmpty()
                                                ->end()
                                                ->scalarNode("format")
                                                    ->cannotBeEmpty()
                                                    ->defaultValue("%s")
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                    ->arrayNode('orderBy')
                                        ->useAttributeAsKey('name')
                                        ->prototype('scalar')
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->scalarNode('form')
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
