<?php

namespace CustomIS\CrudAdminBundle\DependencyInjection;

use CustomIS\CrudAdminBundle\Admin\Admin;
use CustomIS\CrudAdminBundle\Admin\Column;
use CustomIS\CrudAdminBundle\Builder\AdminBuilder;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class CustomISCrudAdminExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        foreach ($config['entities'] as $key => $value)
        {
            $definition = new Definition(Admin::class);
            $definition->setArguments([
                $key,
                $value['entity'],
                new Reference('doctrine')
            ]);

            $definition->addMethodCall('setTitle', [$value['title']]);
            $definition->addTag('crudadmin.' . $value['tag']);

            if (isset($value['list']['repositoryMethod']))
            {
                $definition->addMethodCall('setRepositoryMethod', [$value['list']['repositoryMethod']]);
            }

            if (isset($value['list']['orderBy']))
            {
                $definition->addMethodCall('setOrderBy', [$value['list']['orderBy']]);
            }

            foreach ($value['list']['columns'] as $columnKey => $columnValue)
            {
                $columnDefinition = new Definition(Column::class);
                $columnDefinition->setPublic(false);
                $columnDefinition->setArguments([
                    $columnKey,
                    $columnValue['label'],
                    new Reference('property_accessor'),
                    $columnValue['format']
                ]);

                if (isset($columnValue['property']))
                {
                    $columnDefinition->addMethodCall('setProperty', [$columnValue['property']]);
                }

                if (isset($columnValue['type']))
                {
                    $columnDefinition->addMethodCall('setType', [$columnValue['type']]);
                }

                $container->setDefinition(sprintf('crud.admin.%s.list.column.%s', $key, $columnKey), $columnDefinition);

                $definition->addMethodCall('addColumn', [$columnDefinition]);
            }

            if (isset($value['form']))
            {
                $definition->addMethodCall('setForm', [new Reference($value['form'])]);
            }
            $definition->setPublic(true);

            $container->setDefinition(sprintf('crudadmin.admin.%s', $key), $definition);
        }
    }
}
