<?php

namespace CustomIS\CrudAdminBundle\Controller;

use CustomIS\AppBundle\Annotation\Menu;
use CustomIS\ControllerUtilsBundle\Response\RedirectRouteResponse;
use CustomIS\CrudAdminBundle\Admin\Admin;
use Doctrine\Common\Annotations\Reader;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\Mapping\ClassMetadata;
use Gedmo\Mapping\Annotation\SortablePosition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/crud/{type}", service="CustomIS\CrudAdminBundle\Controller\DefaultController")
 */
class DefaultController
{
    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * @var Reader
     */
    private $reader;

    /**
     * DefaultController constructor.
     *
     * @param ManagerRegistry $managerRegistry
     * @param Reader          $reader
     */
    public function __construct(ManagerRegistry $managerRegistry, Reader $reader)
    {
        $this->managerRegistry = $managerRegistry;
        $this->reader = $reader;
    }

    /**
     * @Route("/list")
     *
     * @param Admin $admin
     *
     * @return array
     */
    public function listAction(Admin $admin)
    {
        $repositoryMethod = $admin->getRepositoryMethod();
        $manager = $this->getEntityManager($admin);
        $metadata = $manager->getClassMetadata($admin->getEntity());

        return [
            'type'       => $admin->getName(),
            'title'      => $admin->getTitle(),
            'columns'    => $admin->getColumns(),
            'data'       => $manager->getRepository($admin->getEntity())->$repositoryMethod(
                [],
                $this->isSortable($metadata)
                    ? [$this->getSortableColumn($metadata) => 'ASC']
                    : $admin->getOrderBy()
            ),
            'primaryKey' => $metadata->getIdentifierFieldNames()[0],
            'sortable'   => $this->isSortable($metadata),
            'editable'   => $admin->hasForm(),
        ];
    }

    /**
     * @Route("/add")
     * @Menu("customis_crudadmin_default_list", label="Přidání položky")
     * @param Request           $request
     * @param Admin             $admin
     * @param FlashBagInterface $flashMessenger
     *
     * @return array|RedirectRouteResponse
     */
    public function addAction(Request $request, Admin $admin, FlashBagInterface $flashMessenger)
    {
        $entityClass = $admin->getEntity();
        $entity = new $entityClass();

        $form = $admin->getForm()->setData($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getEntityManager($admin);
            try {
                $manager->persist($entity);
                $manager->flush();
                $flashMessenger->add("success", "Položka byla úspěšně uložena");

                return new RedirectRouteResponse("customis_crudadmin_default_list", [
                    'type' => $admin->getName(),
                ]);
            } catch (UniqueConstraintViolationException $ex) {
                $flashMessenger->add('error', 'Položka již existuje.');
            } catch (\Exception $ex) {
                $flashMessenger->add('error', 'Při vytvoření došlo bohužel k neočekávané chybě.');
            }
        }

        return [
            'form' => $form->createView(),
            'type' => $admin->getName(),
        ];
    }

    /**
     * @Route("/edit/{id}")
     * @Menu("customis_crudadmin_default_list", label="Úprava položky")
     *
     * @param Request           $request
     * @param Admin             $admin
     * @param FlashBagInterface $flashBag
     * @param string            $id
     *
     * @return array|RedirectRouteResponse
     */
    public function editAction(Request $request, Admin $admin, FlashBagInterface $flashBag, $id)
    {
        $manager = $this->getEntityManager($admin);
        if (($entity = $manager->getRepository($admin->getEntity())->find($id)) === null) {
            throw new NotFoundHttpException('Požadovanou položku nelze upravit, protože nebyla nalezena');
        }

        $form = $admin->getForm()->setData($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $manager->flush();
                $flashBag->add('success', 'Položka byla úspěšně upravena');

                return new RedirectRouteResponse("customis_crudadmin_default_list", [
                    'type' => $admin->getName(),
                ]);
            } catch (UniqueConstraintViolationException $ex) {
                $flashBag->add('error', 'Existuje duplicitní položka, nelze provést změnu.');
            } catch (\Exception $ex) {
                $flashBag->add('error', 'Při změně došlo bohužel k neočekávané chybě.');
            }
        }

        return [
            'form' => $form->createView(),
            'type' => $admin->getName(),
        ];
    }

    /**
     * @Route("/delete/{id}")
     * @param Request           $request
     * @param Admin             $admin
     * @param FlashBagInterface $flash
     * @param string            $id
     *
     * @return RedirectRouteResponse|JsonResponse
     */
    public function deleteAction(Request $request, Admin $admin, FlashBagInterface $flash, $id)
    {
        $manager = $this->getEntityManager($admin);
        if (($entity = $manager->getRepository($admin->getEntity())->find($id)) !== null) {
            $result = true;
            $message = 'Položka byla úspěšně smazána';
            try {
                $manager->remove($entity);
                $manager->flush();
            } catch (ForeignKeyConstraintViolationException $ex) {
                $result = false;
                $message = 'Položka nebyla smazána, protože již byla v systému použita.';
            } catch (\Exception $ex) {
                $result = false;
                $message = 'Při mazaní došlo bohužel k neočekávané chybě.';
                $flash->add('error', $message);
            }
        } else {
            throw new NotFoundHttpException('Požadovaná položka pro smazání nebyla nalezena');
        }
        if ($request->isXmlHttpRequest()) {
            return new \Symfony\Component\HttpFoundation\JsonResponse(['result' => $result, 'message' => $message]);
        }
        if ($result === false) {
            $flash->add('error', $message);
        } else {
            $flash->add('success', $message);
        }

        return new RedirectRouteResponse("customis_crudadmin_default_list", [
            'type' => $admin->getName(),
        ]);
    }

    /**
     * @Route("/sort", condition="request.isXmlHttpRequest()")
     * @Method("POST")
     * @param Request $request
     * @param Admin   $admin
     *
     * @return JsonResponse
     */
    public function sortAction(Request $request, Admin $admin)
    {
        $entityManager = $this->getEntityManager($admin);

        $entity = $entityManager->find($admin->getEntity(), $request->request->get('sortable-id'));
        $entity->setPoradi($entity->getPoradi() + $request->request->getInt('sortable-diff'));

        $entityManager->flush();

        return new JsonResponse(['return' => true]);
    }

    /**
     * @param Admin $admin
     *
     * @return \Doctrine\Common\Persistence\ObjectManager|null|object
     */
    private function getEntityManager(Admin $admin)
    {
        return $this->managerRegistry->getManagerForClass($admin->getEntity());
    }

    /**
     * @param ClassMetadata $classMetadata
     *
     * @return bool|string
     */
    private function getSortableColumn(ClassMetadata $classMetadata)
    {
        static $column;
        if ($column === null) {
            foreach ($classMetadata->getReflectionProperties() as $property) {
                $propertyAnnotations = $this->reader->getPropertyAnnotations($property);
                foreach ($propertyAnnotations as $propertyAnnotation) {
                    if ($propertyAnnotation instanceof SortablePosition) {
                        return ($column = $classMetadata->getColumnName($property->getName()));
                    }
                }
            }
            $column = false;
        }

        return $column;
    }

    /**
     * @param ClassMetadata $classMetadata
     *
     * @return bool
     */
    private function isSortable(ClassMetadata $classMetadata)
    {
        return $this->getSortableColumn($classMetadata) !== false;
    }
}
